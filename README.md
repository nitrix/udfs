[![Build Status](https://travis-ci.org/nitrix/udfs.svg?branch=develop)](https://travis-ci.org/nitrix/udfs)

UDFS is a parasite distributed file system in userspace.

It stores its data on multiple providers by using various steganography methods: from images uploaded on social networks to just about anything else you'd find online.

The result is a virtually unlimited FS, accessible anywhere, with your data encrypted and replicated along with plausible deniability.

This is my take of the popular saying "Everything is data" presented as a useful piece of software.

Questions, feedbacks and contributions are all welcome.