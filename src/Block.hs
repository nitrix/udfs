module Block where

import Data.ByteString

type Identifier = String
type Block      = ByteString
