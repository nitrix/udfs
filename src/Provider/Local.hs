module Provider.Local (providerLocal) where

import Control.Exception (bracket)
import Control.Monad.Trans.Maybe
import qualified Data.ByteString as B
import System.Directory (createDirectoryIfMissing, getAppUserDataDirectory)
import System.IO (openBinaryTempFile, hClose, IOMode(ReadMode), openBinaryFile)
import System.IO.Error (catchIOError, tryIOError)

import Block
import Provider

providerLocal = defaultProvider
    { providerName      = "Local"
    -- , providerBlockSize = 1024 * 1024 -- 1MB
    , providerBlockSize = 10 -- 10B
    , providerCreate    = implCreate
    , providerRead      = implRead
    }
 
implCreate :: Block -> MaybeT IO Identifier
implCreate block = MaybeT $ catchIOError (bracket acquire release process) (const $ return Nothing)
    where
        acquire = do
            dir <- getAppUserDataDirectory "udfs"
            createDirectoryIfMissing True $ dir ++ "/local"
            openBinaryTempFile (dir ++ "/local") ""
        release (filepath, handle) = hClose handle
        process (filepath, handle) = do
            B.hPut handle block
            return . Just . extractIdentifier $ filepath
        extractIdentifier = reverse . takeWhile (not . (== '/')) . reverse
                
implRead :: Identifier -> MaybeT IO Block
implRead identifier = MaybeT $ catchIOError (bracket acquire release process) (const $ return Nothing)
    where
        acquire = do
            dir <- getAppUserDataDirectory "udfs"
            openBinaryFile (dir ++ "/local/" ++ identifier) ReadMode
        release = hClose
        process handle = Just <$> B.hGetContents handle
