module Provider where

import Block
import Control.Monad.Trans.Maybe
import Control.Monad

data Provider = MkProvider
    { providerName      :: String
    , providerBlockSize :: Int
    , providerCreate    :: Block -> MaybeT IO Identifier
    , providerRead      :: Identifier -> MaybeT IO Block
    , providerUpdate    :: Identifier -> Block -> MaybeT IO Identifier
    , providerDelete    :: Identifier -> IO Bool
    }

defaultProvider = MkProvider
    { providerName      = "Default"
    , providerBlockSize = 0
    , providerCreate    = \_   -> MaybeT $ return Nothing
    , providerRead      = \_   -> MaybeT $ return Nothing
    , providerUpdate    = \_ _ -> MaybeT $ return Nothing
    , providerDelete    = \_   -> return False
    }

instance Show Provider where
    show = providerName
