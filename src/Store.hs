module Store ( Store
             , storeCreate
             {--
             , storeLookup
             , storePartialLookup
             , storeInsert
             , storeDelete
             , storeTruncate
             , storeExist
             --}
             ) where

import Prelude hiding (null, splitAt, take, drop)
import System.Random.Shuffle
import Control.Monad.Trans.Maybe
import Data.ByteString
-- import Control.Lens
import Block
import Provider
import qualified Data.Map as M
import Data.Set (elems)

type Key    = String
type Value  = ByteString
type Offset = Integer
type Length = Integer

data Store = MkStore { storeRoots :: M.Map Provider Identifier }

storeCreate :: Store -> Key -> Value -> IO Store
storeCreate store _ _ = return store

{-
storeCreateBlocks _ "" = return []
storeCreateBlocks [] _ = return []
storeCreateBlocks providers value = go providers value
    where
        go :: [Provider] -> Value -> IO [(Provider, Identifier)] 
        go [] _ = return []
        go (p:ps) value = do 
            identifier <- runMaybeT $ providerCreate p block
            case identifier of
                Just x  -> do
                    xs <- storeCreateBlocks providers rest
                    return $ (p, x) : xs
                Nothing -> go ps value
            where
                rest  = drop (providerBlockSize p) value
                block = take (providerBlockSize p) value
-}

{-
storeLookup :: Store -> Key -> IO (Maybe Value)
storeLookup = undefined

storePartialLookup :: Store -> Key -> Offset -> Length -> IO (Maybe Value)
storePartialLookup = undefined

storeInsert :: Store -> Key -> Value -> IO Store
storeInsert = undefined

storeDelete :: Store -> Key -> IO (Maybe Store)
storeDelete = undefined

storeTruncate :: Store -> Key -> Offset -> IO (Maybe Store)
storeTruncate = undefined

storeExist :: Store -> Key -> IO Bool
storeExist = undefined

storeRandomProviders :: Store -> IO [Provider]
storeRandomProviders = shuffleM . elems . M.keysSet . roots
-}
